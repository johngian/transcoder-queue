import { Transform } from "stream";
import ffmpeg from "fluent-ffmpeg";

class FileFilter extends Transform {
  constructor() {
    super({
      readableObjectMode: true,
      writableObjectMode: true,
    });
  }

  _transform(chunk, encoding, next) {
    if (!chunk.stats.isDirectory()) {
      return next(null, chunk);
    }

    return next();
  }
}

class MKVFilter extends Transform {
  constructor() {
    super({
      readableObjectMode: true,
      writableObjectMode: true,
    });
  }

  _transform(chunk, encoding, next) {
    if (chunk.path.endsWith(".mkv")) {
      return next(null, chunk);
    }
    return next();
  }
}

class AACFilter extends Transform {
  constructor() {
    super({
      readableObjectMode: true,
      writableObjectMode: true,
    });
  }

  _transform(chunk, encoding, next) {
    var command = ffmpeg.ffprobe(chunk.path, function (err, metadata) {
      const aac_streams = metadata.streams.filter(
        (entry) => entry.codec_name === "aac"
      );
      if (aac_streams.length === 0) {
        return next(null, chunk);
      }
      return next();
    });

    setTimeout(function () {
      command.on("error", function () {
        console.log("Ffmpeg has been killed");
        return next();
      });

      command.kill();
    }, 60000);
  }
}

export { FileFilter, MKVFilter, AACFilter };
