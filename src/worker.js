import { getVideoQueue } from "./helpers.js";
import { processVideo } from "./tasks.js";

function main() {
  const videoQueue = getVideoQueue();
  videoQueue.process("video", processVideo);

  videoQueue.on("error", function (error) {
    console.log(`Something wrong happened: ${error}`);
  });

  videoQueue.on("waiting", function (jobId) {
    console.log(`Waiting for ${jobId}`);
  });

  videoQueue.on("failed", function (job, err) {
    console.log(`Job with id ${job.id} failed with error: ${err}`);
  });
}

main();
