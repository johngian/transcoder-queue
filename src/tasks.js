import FFmpegCommand from "fluent-ffmpeg";
import { rename as fileRename } from "fs";

function processVideo(job, done) {
  console.log(`Received job #${job.id}: ${JSON.stringify(job.data)}`);
  var cmd = new FFmpegCommand();

  cmd
    .input(job.data.input_path)
    .output(job.data.output_path)
    .audioCodec("aac")
    .videoCodec("copy")
    .on("start", function (commandLine) {
      console.log(`Spawned Ffmpeg with command: ${commandLine}`);
    })
    .on("error", function (err) {
      console.log(`Cannot process video: ${err.message}`);
      throw new Error(err.message);
    })
    .on("end", function () {
      console.log("Transcoding succeeded !");
      fileRename(job.data.output_path, job.data.input_path, function (err) {
        if (err) {
          console.log("Something went wrong while renaming!");
          throw new Error(err);
        }
      });
      done();
    })
    .run();
}

export { processVideo };
