import klaw from "klaw";
import { join as pathJoin } from "path";
import { v4 as uuidv4 } from "uuid";
import { getVideoQueue } from "./helpers.js";
import { settings } from "./config.js";
import { FileFilter, MKVFilter, AACFilter } from "./filters.js";

const videoQueue = getVideoQueue();

function main() {
  for (const path of settings.INPUT_FILE_PATHS) {
    console.log(`Master: Adding files from path: ${path}`);
    let dir_filter = new FileFilter();
    let mkv_filter = new MKVFilter();
    let aac_filter = new AACFilter();
    klaw(path)
      .pipe(dir_filter)
      .pipe(mkv_filter)
      .pipe(aac_filter)
      .on("data", function (chunk) {
        const job = {
          input_path: chunk.path,
          output_path: pathJoin(settings.TMP_FILE_PATH, `${uuidv4()}.mkv`),
        };
        console.log(`Filter: Adding job: ${JSON.stringify(job)}`);
        videoQueue.add("video", job);
      })
      .on("error", (err, item) => {
        console.log(err.message);
        console.log(`Failed while walking: ${item.path}`);
      })
      .on("end", () => {
        console.log("End walking files");
      });
  }
}

main();
