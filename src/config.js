import config from "dotenv";

config.config();

const castDefault = (value) => value;
const castCSV = (value) => value.split(",").map((s) => s.trim());

const configEntry = (key, defaultValue, cast = castDefault) =>
  process.env[key] ? cast(process.env[key]) : defaultValue;

const settings = {
  REDIS_URL: configEntry("TRANSCODER_REDIS_URL"),
  INPUT_FILE_PATHS: configEntry(
    "TRANSCODER_INPUT_FILE_PATHS",
    undefined,
    castCSV
  ),
  TMP_FILE_PATH: configEntry("TRANSCODER_TMP_FILE_PATH"),
};

export { settings };
