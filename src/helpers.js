import Queue from "bull";
import { settings } from "./config.js";

function getVideoQueue() {
  return new Queue("video transcoding", settings.REDIS_URL);
}

export { getVideoQueue };
