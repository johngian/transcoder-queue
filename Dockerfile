FROM node:current-buster

WORKDIR /app
COPY package.json /app
COPY package-lock.json /app

RUN apt-get update && apt-get install -y ffmpeg wait-for-it
RUN npm install

COPY . /app
